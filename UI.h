//
// Created by tever on 11/01/2021.
//

#ifndef COURSEWOLRK_UI_H
#define COURSEWOLRK_UI_H
#ifndef STOCK_H
#define Stock_H
#include <iostream>
#include <string>
#include <plotter.h>
#include <utility>
#include <vector>
#include "Junker.h"
#include "Storage.h"
using namespace std;
Janitor jn = new Janitor;
storage cart = new storage;
class Printer{
private:
public:
    void printreceipt();
    static void printfield(int x);
    void title();
    static string element(string x, int fullsize);
    static void footer();

};

void Printer::title() {

    cout << element("ID",3)
         <<element("name",12)
         <<element("type",7)
         <<element("cost",6)
         <<element("quantity",12)
         <<element("Total Item cost",15)
         <<"|\n";
}

string Printer::element(string x, int fullsize) {
    int size = x.size();
    int paddingsz = fullsize - size;
    string padding;
    for (i = 0; i < paddingsz; i++){ //i++ means add 1 to i each iteration
        padding = padding  + " "; //print 5 times
    }
    return "|" + x + padding;
}

void Printer::printfield(int x) {
    cout << element(cart.get_id(x),3)
        <<element(cart.get_name(x),12)
        <<element(cart.get_type(x),7)
        <<element(cart.get_cost(x),6)
        <<element(cart.get_quantity(x),12)
        <<element(cart.get_totalCost(x),15)
}
void Printer::footer() {
    cout << element(" ",12)
    << "total number of items: "
    << to_string(cart.get_itemstotal())
    << "   Total Cost: £ "
    << to_string(cart.get_totalCost())
    << "\n";
}

void Printer::printreceipt() {
    int pos;
    title();
    for (i = 0; i < pos; i++){ //i++ means add 1 to i each iteration
        printfield(pos); //print 5 times
    }
    footer();
}


#endif //COURSEWOLRK_UI_H
