//
// Created by teveredi on 28/12/2020.
//
#ifndef STOCK_H
#define Stock_H
#include <iostream>
#include <string>
#include <plotter.h>
#include <utility>
#include <vector>
#include "Junker.h"
using namespace std;

class itemtest {
protected:
    string name;
    double cost{};
public:
    itemtest();
    itemtest(string name, double cost);
    double get_cost() const;
    string get_name() const;
    void set_cost(double amount);
    void set_name(string newname);
    virtual void print();
    virtual string val();
};

itemtest::itemtest() {
    set_name("test obj");
    set_cost(0.00);
}
itemtest::itemtest(string name, double cost){
    cout << " object is created ";
    set_name(move(name));
    set_cost(cost);
}
//getters
double itemtest::get_cost() const{
    return cost;
}
string itemtest::get_name() const{
    return name;
}
//setters
void itemtest::print() {
    string x = "name: " + name + "  Cost: " + to_string(cost);
    cout << x;
}

void itemtest::set_cost(double amount) {
    this->cost = amount;
}
string itemtest::val() {
    string x = "" + name + "," + to_string(cost);
    return x;
}

void itemtest::set_name(string newname) {
    this->name = move(newname);

}


class Stock : public itemtest{
private:
    int quantity;
    double totalprice;
public:
    Stock(string name, double cost, int quantity );
    ~Stock();
//functions
    int get_quantity() const;
    string get_cost();
    string val() override;
};

Stock::Stock(string name, double cost, int quantity) {
    cout << "Stock object is created";
    itemtest::set_name(name);
    itemtest::set_cost(cost);

    }
int  Stock::get_quantity() const{
    return quantity;
}
string Stock::val() {
    string x = itemtest::val() + "," + to_string(quantity) + "," + to_string(totalprice);
    return x;
}

string Stock::get_cost() {
    return to_string(cost);
}
Stock::~Stock() = default;

class  Books: public Stock{
private:
    string author;
    string genre;
    string publisher;
public:
    string get_author();
    string get_genre();
    string get_publisher();
};



# endif